//import { Component, OnInit } from '@angular/core';
import {Post} from './post'
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'jce-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
   inputs:['post']
})
export class PostComponent implements OnInit {
post:Post;
isEdit : boolean = false;
editButtonText = 'Edit';
 @Output() deleteEvent = new EventEmitter<Post>();
  constructor() { }
  sendDelete(){
    this.deleteEvent.emit(this.post);
 }
 toggleEdit(){
    //update parent about the change
    this.isEdit = !this.isEdit; 
     this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit';    
  }
  ngOnInit() {
  }

}
