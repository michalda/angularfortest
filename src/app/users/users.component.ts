import { Component, OnInit } from '@angular/core';
import {UsersService} from './users.service';

@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
  styleUrls:  [`
    .users table { cursor: default; }
    .users li:hover { background: #ecf0f1; } 
   .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class UsersComponent implements OnInit {
  title1='new component';
   users;
   currentUser;
   isLoading = true;

  select(user){
  this.currentUser = user; 
  console.log(	this.currentUser);
 }
 deleteUser(user){
    this.users.splice(
      this.users.indexOf(user),1
    )
  }
  constructor (private _usersService: UsersService) { 
        //    this.users = this._usersService.getUsers();
   }
   addUser(user){
    this.users.push(user)
  }

   editUser(originalAndEdited){
    this.users.splice(
     this.users.indexOf(originalAndEdited[0]),1,originalAndEdited[1]  
    )
    console.log(this.users);
  }  
 
  

  ngOnInit() {
    this._usersService.getUsers()
		    .subscribe(users => {this.users = users;
                              this.isLoading = false});
  }

}
