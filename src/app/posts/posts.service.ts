import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import 'rxjs/add/operator/delay';

@Injectable()
export class PostsService {
/*posts= [
    {author :'John',content :'aaaaaaa',title:'dsfdss'},
    {author :'lolo',content :'bbbbbb',title:'dfdsf'},
    {author :'jojo',content :'ccccc',title:'jhgjhgj'},
     {author :'daty',content :'bbbbbb',title:'dfdsf'}
    ]*/
     private _url = "http://jsonplaceholder.typicode.com/posts";
  
     constructor(private _http: Http) { }

    getPosts(){
      		return this._http.get(this._url)
			.map(res => res.json()).delay(2000);
	
	}



}
