import { Component, OnInit } from '@angular/core';
import {PostsService} from './posts.service';

@Component({
  selector: 'jce-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
title4='Post Work';
posts;
   isLoading = true;
   
  constructor(private _postsService: PostsService) 
  { 
          //  this.posts = this._postsService.getPosts();
   }
   deletePost(post){
   this.posts.splice(
      this.posts.indexOf(post),1
    )
  }
  addPost(post){
    this.posts.push(post)
  }


  ngOnInit() {
  this._postsService.getPosts()
		    .subscribe(posts => {this.posts = posts;
                              this.isLoading = false});
  }

}
