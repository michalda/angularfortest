//import { Component, OnInit } from '@angular/core';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {NgForm} from '@angular/forms';
import {User} from '../user/user'

@Component({
  selector: 'jce-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  
  @Output() userAddedEvent = new EventEmitter<User>();
  user:User = {
    Id:'',
    Name: '',
    Email: ''
  };

  constructor() { }
  onSubmit(form:NgForm){
   console.log(form);
    this.userAddedEvent.emit(this.user);
    this.user = {
         Id:'',
       Name: '',
       Email: ''
    }
  }

  ngOnInit() {
  }

}
