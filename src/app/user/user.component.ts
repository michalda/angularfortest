import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {User} from './user'

@Component({
  selector: 'jce-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  inputs:['user']
})

export class UserComponent implements OnInit {
  @Output() deleteEvent = new EventEmitter<User>();
  @Output() editEvent = new EventEmitter<User[]>();
  user:User;
  tempUser:User = {Id:null,Email:null,Name:null};
  isEdit : boolean = false;
  editButtonText = 'Edit';

  constructor() { }
  sendDelete(){
    this.deleteEvent.emit(this.user);
  }

  cancelEdit(){
    this.isEdit = false;
     this.user.Id = this.tempUser.Id;
    this.user.Email = this.tempUser.Email;
    this.user.Name = this.tempUser.Name;
    this.editButtonText = 'Edit'; 
  }
  
  toggleEdit(){
     //update parent about the change
     this.isEdit = !this.isEdit; 
     this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit';   
     if(this.isEdit){
       this.tempUser.Email = this.user.Email;
       this.tempUser.Name = this.user.Name;
     } else {
       let originalAndNew = [];
       originalAndNew.push(this.tempUser,this.user);
       this.editEvent.emit(originalAndNew);
     }
  }

  ngOnInit() {
  }

}