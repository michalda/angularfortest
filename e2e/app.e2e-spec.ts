import { ForTestPage } from './app.po';

describe('for-test App', function() {
  let page: ForTestPage;

  beforeEach(() => {
    page = new ForTestPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
